const balloonTemplate = `
	<div class="baloon-layout">
			{% if properties.title || properties.image %}
			<div class="baloon-layout__header" style="background-image: url({{ properties.image }})">
				{% if properties.title %}<h3 class="baloon-layout__title">{{ properties.title }}</h3>{% endif %}
			</div>
			{% endif %}
			<ul class="baloon-layout__props">
				{% if properties.address %}<li class="baloon-layout__prop address">{{ properties.address }}</li>{% endif %}
				{% if properties.metro %}<li class="baloon-layout__prop metro">{{ properties.metro }}</li>{% endif %}
				{% if properties.tel %}<li class="baloon-layout__prop tel">{{ properties.tel }}</li>{% endif %}
				{% if properties.worktime %}<li class="baloon-layout__prop worktime">{{ properties.worktime|raw }}</li>{% endif %}
			</li>
			<span class="close">&times;</span>
	</div>`;

const balloonMethods = {
	build: function build() {
		this.constructor.superclass.build.call(this);

		this._$element = $('.baloon-layout', this.getParentElement());
		this.applyElementOffset();
		this._$element
			.find('.close')
			.on('click', $.proxy(this.onCloseClick, this));
	},

	applyElementOffset: function applyElementOffset() {
		const arrowHeight = 10;
		this._$element.css({
			left: -(this._$element[0].offsetWidth / 2),
			top: -(this._$element[0].offsetHeight + arrowHeight),
		});
	},

	onCloseClick: function onCloseClick(e) {
		e.preventDefault();

		this.events.fire('userclose');
	},
};

export default {
	create: function create() {
		const BalloonContentLayoutClass = ymaps.templateLayoutFactory.createClass(balloonTemplate, balloonMethods);
		ymaps.layout.storage.add('ndv#baloonLayout', BalloonContentLayoutClass);

		// let iconLayout = ymaps.templateLayoutFactory.createClass(iconTemplate, iconMethods);
		// ymaps.layout.storage.add('ndv#iconLayout', iconLayout);
	},

	balloonOptions: {
		balloonShadow: false,
		balloonLayout: 'ndv#baloonLayout',
		balloonPanelMaxMapArea: 0,
	},

	defaultIcon: {
		iconLayout: 'default#image',
		iconImageHref: '/pic/design/map-office.png',
		iconImageSize: [59, 47],
		iconImageOffset: [-30, -47],
	},
};
