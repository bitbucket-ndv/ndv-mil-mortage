import scrollbar from 'perfect-scrollbar';
import slick from 'slick-carousel';
import applicationForm from './ApplicationForm';
import map from './MapSection';
import slides from './Slides';
import faq from './Faq';
import actions from './actions/';

/*
	Скролл для левой части на главной странице
 */
scrollbar.initialize(document.querySelector('.information'), {suppressScrollX: true});

/*
	Форма "Отправить заявку"
 */
applicationForm.initialize();

/*
	Секция с картой
	Выезжает слева при наведении на вертикальный разделитель
 */
map.initialize();

/*
	Слайды с объектами
 */
slides.initialize();

/*
	Задаем обработчики для кнопок панорама/камеры/видео в слайдере
 */
actions.initialize();

/*
	Инициализируем блок "Вопросы и ответы"
	ps. Раскрытие можно реализовать на CSS, но требует дополнительных элементов (чекбоксов)
 */
faq.initialize();
faq.on('close', () => {
	// Обновляю прокрутку, чтобы не появлялось пустого места внизу блока после скрытия ответа
	scrollbar.update(document.querySelector('.information'));
});

$('#information-slider').slick({
	accessibility: false,
	autoplay: true,
	prevArrow: '<i class="prev"></i>',
	nextArrow: '<i class="next"></i>',
});

$('#information-slider').on('click', function() {
	// $(this)
});
