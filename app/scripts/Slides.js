import scrollbar from 'perfect-scrollbar';
import assign from 'object-assign';
import EventEmitter from 'events';

let isOpen = false;
let activeSlideNum;

const slider = assign(new EventEmitter(), {
	initialize: function initialize() {
		this.container = document.querySelector('.office-slides');
		this.items = Array.prototype.slice.call(this.container.querySelectorAll('.slide'));
		this.bindHandlers();

		// Выбираем блоки с текстом из карточек объектов в слайдере и инициализируем плагин
		const $cardTextItems = $('.card .card__text');
		$cardTextItems.each((i, item) => {
			scrollbar.initialize(item, {suppressScrollX: true});
		});

		this.on('open', () => {
			$cardTextItems.each((i, item) => {
				scrollbar.update(item, {suppressScrollX: true});
			});
		});
	},

	bindHandlers: function bindHandlers() {
		this.container.addEventListener('transitionend', slider.onTransitionEnd);
		this.container.querySelector('.control-prev').addEventListener('click', slider.prev);
		this.container.querySelector('.control-next').addEventListener('click', slider.next);
		this.container.querySelector('.close').addEventListener('click', slider.toggleState);
		$('.offer-list').on('click', '.offer-list__item', this.handleClick);
		$('.information [data-slide]').click(this.handleClick);
	},

	setActiveSlide: function setActiveSlide(num) {
		activeSlideNum = num;
		const len = this.items.length;
		this.items.forEach((item, i) => {
			let cls = item.className.replace(/\s+(active|next|prev)/ig, '');
			if ((num === len - 1 && i === 0) || i === num + 1) {
				cls += ' next';
			}
			if (num === 0 && i === len - 1 || i === num - 1) {
				cls += ' prev';
			}
			if (num === i) {
				cls += ' active';
			}
			item.className = cls;
		});
	},

	next: function next() {
		slider.setActiveSlide(activeSlideNum === slider.items.length - 1 ? 0 : activeSlideNum + 1);
	},

	prev: function prev() {
		slider.setActiveSlide(activeSlideNum === 0 ? slider.items.length - 1 : activeSlideNum - 1);
	},

	onTransitionEnd: function onTransitionEnd(event) {
		if (event.target.className.indexOf('slider') === -1) {
			return;
		}
		if (isOpen) {
			slider.container.className += ' is-animate';
		} else {
			slider.container.className = slider.container.className.replace(/\s+is-animate/ig, '');
		}
	},

	handleClick: function handleClick(event) {
		event.stopPropagation();
		event.preventDefault();

		const slideId = +this.getAttribute('data-slide');
		slider.setActiveSlide(slideId);
		slider.toggleState();
	},

	toggleState: function toggleState() {
		(isOpen ? slider.close() : slider.open());
	},

	open: function open() {
		isOpen = true;
		$(this.container).addClass('is-open');
		setTimeout(() => {
			$(this.container).addClass('slider-animate');
			slider.emit('open');
		}, 10);
	},

	close: function close() {
		isOpen = false;
		$(this.container).one('transitionend', () => {
			$(this.container).removeClass('is-open');
		});
		$(this.container).removeClass('slider-animate');
	},
});

export default slider;
