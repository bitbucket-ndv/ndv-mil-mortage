import assign from 'object-assign';
import EventEmitter from 'events';

let faq = assign(new EventEmitter(), {
	initialize: function initialize() {
		$('.questions').on('click', '.questions__title', faq.handleItemClick);
	},

	handleItemClick: function handleItemClick() {
		let $item = $(this.parentNode);
		$item.toggleClass('is-open');

		if ($item.hasClass('is-open')) {
			faq.emit('open');
		} else {
			faq.emit('close');
		}
	},
});

export default faq;
