import gulp from 'gulp';
import imagemin from 'gulp-imagemin';

gulp.task('copy:fonts', () => {
	return gulp
		.src('app/fonts/*')
		.pipe(gulp.dest('dist/fonts'));
});

gulp.task('copy:js', () => {
	return gulp
		.src('app/scripts/map/points.js')
		.pipe(gulp.dest('dist/jscripts'));
});

gulp.task('copy:images', () => {
	return gulp
		.src(['app/images/*.{png,gif,jpg}', 'app/images/slides/*'])
		.pipe(imagemin({
			optimizationLevel: 3,
		}))
		.pipe(gulp.dest('dist/pic/design'));
});

gulp.task('copy', ['copy:images', 'copy:js']);
