export default {
	dist: 'dist',
	images: 'dist/pic/design',
	styles: 'dist/css',
	scripts: 'dist/jscripts'
}
